/**
 * <p>
 * Provides a small framework for command line UI's.
 * </p>
 * 
 * @author Joel Håkansson
 */
package org.daisy.cli;