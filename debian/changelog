brailleutils (1.2.3-6) unstable; urgency=medium

  [ Samuel Thibault ]
  * control: Update alioth list domain.

  [ Debian Janitor ]
  * Trim trailing whitespace.
  * Bump debhelper from deprecated 7 to 12.
  * Set debhelper-compat version in Build-Depends.
  * Fix day-of-week for changelog entry 1.2~b-2.
  * Update standards version to 4.5.0, no changes needed.

 -- Samuel Thibault <sthibault@debian.org>  Sun, 01 Nov 2020 01:35:48 +0100

brailleutils (1.2.3-5) unstable; urgency=medium

  * control: Make libbrailleutils-java-doc Multi-Arch: foreign.
  * control: Set Vcs-* to salsa.debian.org.
  * control: Bump Standards-Version to 4.4.1 (no changes).
  * watch: Generalize pattern.

 -- Samuel Thibault <sthibault@debian.org>  Mon, 14 Oct 2019 00:29:19 +0200

brailleutils (1.2.3-4) unstable; urgency=medium

  [ Samuel Thibault ]
  * control: Bump Standards-Version to 4.2.0 (no changes).
  * control: Move maintenance to a11y team.

  [ Emmanuel Bourg ]
  * Fixed the build failure with Java 10 (Closes: #897482)

 -- Samuel Thibault <sthibault@debian.org>  Thu, 27 Dec 2018 14:53:41 +0100

brailleutils (1.2.3-3) unstable; urgency=medium

  * Use canonical anonscm vcs URL.
  * control: Migrate priority to optional.
  * control: Bump Standards-Version to 4.1.4

 -- Samuel Thibault <sthibault@debian.org>  Sat, 28 Apr 2018 17:38:37 +0200

brailleutils (1.2.3-2) unstable; urgency=medium

  * Team upload.
  * watch: Update to github repository.
  * Bump Standards-Version to 3.9.6 (no changes).
  * Fix upstream URL.
  * control: Make brailleutils depend on default-jre-headless instead of
    default-jre.
  * rules: Also install pom file for brailleutils-catalog.

 -- Samuel Thibault <sthibault@debian.org>  Sun, 20 Dec 2015 22:49:23 +0100

brailleutils (1.2.3-1) unstable; urgency=low

  [ Sebastian Humenda ]
  * import new upstream version
  * fix old class path problem

  [ Samuel Thibault ]
  * Bump Standards-Version to 3.9.5 (no changes).

 -- Sebastian Humenda <shumenda@gmx.de>  Tue, 11 Mar 2014 22:25:49 +0200

brailleutils (1.2.1+trunk-1) unstable; urgency=low

  * import new upstream version to fix issues related to classpath and jar
   creation

 -- Sebastian Humenda <shumenda@gmx.de>  Fri, 23 Aug 2013 13:41:40 +0200

brailleutils (1.2.1-3) unstable; urgency=low

  * add breaks/replaces field to brailleutils (Closes: #718007)
  * integrate library into the maven-repo using maven-repo-helper

 -- Sebastian Humenda <shumenda@gmx.de>  Fri, 16 Aug 2013 20:32:14 +0200

brailleutils (1.2.1-2) unstable; urgency=low

  * add default-jre as dependency for package brailleutils to have the "java"
    command available

 -- Sebastian Humenda <shumenda@gmx.de>  Wed, 29 May 2013 19:35:42 +0200

brailleutils (1.2.1-1) unstable; urgency=low

  [ Sebastian Humenda ]
  * updated to upstream v1.2.1
  * separate front end for brailleutils (available as command with the same
    name)
  * wrote man page for brailleUtils
  * improved debian packaging: moved installation from debian/rules to
    *.install, *.dirs

  [ Samuel Thibault ]
  * control: Add missing junit4 dependency.

 -- Sebastian Humenda <shumenda@gmx.de>  Thu, 11 Apr 2013 18:45:11 +0200

brailleutils (1.2~b-2) unstable; urgency=low

  [ Samuel Thibault ]
  * control: Bump Standards-Version to 3.9.3 (no changes).

  [ Sebastian Humenda ]
  * Fixed a problem with broken symlinks in /usr/share/java

  [ Giovanni Mascellani ]
  * Non-maintainer upload.
  * Remove libwoodstox-java dependency.

 -- Sebastian Humenda <shumenda@gmx.de>  Fri, 02 Mar 2012 21:21:13 +0200

brailleutils (1.2~b-1) unstable; urgency=low

  * Initial release, packaged as a dependency for odt2braille. (Closes: #630200)

 -- Sebastian Humenda <shumenda@gmx.de>  Sat, 27 Aug 2011 12:21:13 +0200
